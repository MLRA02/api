const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");
const auth = require("../auth.js");
const mongoose = require("mongoose")



module.exports.addToCart = async (data) => {
    let product = await Product.findById(data.productId);
    if(!product.isActive) {
        return `Product is not available.`
    } else {
        let userCart = await Cart.findOne({userId: data.userId});
        if(!userCart) {
            userCart = new Cart({
                userId: data.userId,
                products: [{
                    productId: data.productId,
                    quantity: data.quantity
                }]
            });
        } else {
            let existingProduct = userCart.products.find((p) => p.productId === data.productId);
            if(existingProduct) {
                existingProduct.quantity += data.quantity;
            } else {
                userCart.products.push({
                    productId: data.productId,
                    quantity: data.quantity
                });
            }
        }

        try {
            await userCart.save();
            return true;
        } catch (error) {
            return false;
        }
    }
};



module.exports.checkout = async (data) => {
    if(!data.isAdmin){
        let product = await Product.findById(data.productId);
        if(!product.isActive) {
            return `Product is not available.`
        } else {
            let newOrder = new Order({
                userId : data.userId,
                products : [{
                    productId: data.productId,
                    quantity : data.quantity
                }],
                totalAmount: product.price * data.quantity
            });

            try {
                await newOrder.save();
                return true;
            } catch (error) {
                return false;
            }
        }
    } else {
        return false;
    }
};

module.exports.getAllOrders = (data) => {

	if (data.isAdmin){
		return Order.find({}).then((result, error) => {
			if (error){
				return false
			} else {
				return result;
			}
		})

	} else {
		return false;
	}
}









